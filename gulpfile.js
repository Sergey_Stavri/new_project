const 
    gulp = require('gulp');
    rename = require('gulp-rename');
    sass = require('gulp-sass');
    autoprefixer = require('gulp-autoprefixer');
    sourcemaps = require('gulp-sourcemaps');
    browserSync = require('browser-sync') .create();
    pug = require('gulp-pug');
    imagemin = require('gulp-imagemin');
    concat = require('gulp-concat');




function css_style(done) {

    gulp.src('./src/style/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            errorLogToConsole: true, 
            outputStyle: 'compressed'
        }))
        .on('error', console.error.bind(console)) 
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({suffix:'.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(concat('./all.css'))  
        .pipe( gulp.dest('./public/styles/'))
        
        .pipe(browserSync.stream()); 
    
    
        done();
}

function sync(done) {
    browserSync.init({
        server: {
            baseDir: "public/",
            index: "HOME.html",
    },
        port: 3000
    });
    done();
}
function browserReload(done) {
    browserSync.reload();
    done();
}


/*function print(done) {
    console.log("Hi");
    done();
}*/

function watchSass() {
    gulp.watch('./**/*.scss',css_style);
}

function watchFiles() {
    gulp.watch("./**/*.scss",css_style);
    gulp.watch("./**/*.html",browserReload);
    gulp.watch("./**/*.scss",browserReload)
    gulp.watch("./**/*.pug",buildHTML);
    gulp.watch("./**/*.js",browserReload);
   // gulp.watch("./public/images/*.png",browserReload);
}
function buildHTML(done) {
    return gulp.src('./src/templates/pages/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('./public/'));    
      // Your options in here.
  };
 
exports.imagemin = () => (
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/images'))
            .pipe(imagemin([
                imagemin.gifsicle({interlaced: true}),
                imagemin.mozjpeg({quality: 75, progressive: true}),
                imagemin.optipng({optimizationLevel: 5}),
                imagemin.svgo({
                    plugins: [
                        {removeViewBox: true},
                        {cleanupIDs: false}
        ]
    })
])));


gulp.task(css_style);
//gulp.task(print);

gulp.task ('default', gulp.parallel(watchFiles, buildHTML,sync));
gulp.task(sync);
gulp.task(buildHTML);








// exports.default=defaultSomeTask;

